/* eslint-disable */
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const DogSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  img: {
    type: String,
    required: true
  },
  level: {
    type: Number,
    default: 1
  },
  hp: {
    type: Number,
    required: true
  },
  damage: {
    type: Number,
    required: true
  },

  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Dog = mongoose.model("dogs", DogSchema);
