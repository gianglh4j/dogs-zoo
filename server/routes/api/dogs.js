const express = require("express");
const router = express.Router();

const Dog = require("../../models/Dog");


// @route POST api/dogs/createdog

router.post("/createdog", (req, res) => {
  

    Dog.findOne({ name: req.body.name }).then(dog => {
    if (dog) {
      return res.status(400).json({ name: "Dog's name already exists" });
    } else {
      const newDog = new Dog({
        name: req.body.name,
        type: req.body.type,
        img: req.body.img,
        damage:req.body.damage,
        hp:req.body.hp
      });
      newDog
            .save()
            .then(dog => res.json(dog))
            .catch(err => console.log(err));

    }
  });
});
router.get("/dogs", (req, res) => {
    Dog.find({}, function(err, dogs) {
       
        res.json(dogs);  
      });

});


  module.exports = router;