import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import Select from "react-select";

const dogOptions = [
  { id: 1, label: "Chihuahua" },
  { id: 2, label: "Bulldog" },
  { id: 3, label: "Dachshund" },
  { id: 4, label: "Akita" }
].map(dogOption => ({
  value: dogOption.label,
  label: dogOption.label
}));

export default function AddDogForm(props) {
  const [open, setOpen] = React.useState(false);
  const [values, setValues] = React.useState({
    name: "",
    type: "",
    img: "",
    hp: 0,
    damage: 0
  });

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    props.handleSubmitForm(values);
    setOpen(false);
  }
  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };
  function handleChangeSelectType(value) {
    setValues({ ...values, type : value.value });
  }
 
  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        ADD BOSS
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Add Dog</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name ..."
            value={values.name}
            onChange={handleChange("name")}
            fullWidth
          />
          <Select
            inputId="type-of-dog"
            TextFieldProps={{
              label: "Type of dog",
              InputLabelProps: {
                htmlFor: "type-of-dog",
                shrink: true
              }
            }}
            placeholder="Choose a type of Boss"
            options={dogOptions}
            value={values.type}
             onChange ={handleChangeSelectType}
          />
          <TextField
            margin="dense"
            id="img"
            label="Image URL ..."
            value={values.img}
            onChange={handleChange("img")}
            fullWidth
          />
          <TextField
            margin="dense"
            id="hp"
            label="Helth Point ..."
            value={values.hp}
            onChange={handleChange("hp")}
            fullWidth
          />
          <TextField
            margin="dense"
            id="damage"
            label="Damge attack ..."
            value={values.damgage}
            onChange={handleChange("damage")}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Subscribe
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
