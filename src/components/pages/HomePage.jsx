import React from "react";
//import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DogCard from "./DogCard";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import AddDogForm from './AddDogForm';
/*const useStyles = makeStyles(theme => ({
    button: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
  })); */

export default function HomePage() {
   

  const Dogs = [
    {
      id: "3bf159b4-dbbb-4d1d-8ee3-3e5610242eb2",
      name: "Hyena",
      type: "Chihuahua",
      img: require("../../images/chihuahua1.jpg")
    },
    {
      id: "9a07f04f-c05a-471d-80e1-5d8af50bbaea",
      name: "Bandicoot",
      type: "Bulldog",
      img: require("../../images/pulldog1.jpg")
    },
    {
      id: "d3bfb6ea-c235-4b0f-a0d1-03a188d5d4cb",
      name: "sloth",
      type: "Bulldog",
      img: require("../../images/pullgod2.jpg")
    },
    {
      id: "bd9a74ae-64bb-4f39-9319-651b3f3346dd",
      name: "bat",
      type: "Bulldog",
      img: require("../../images/pulldog3.jpg")
    },
    {
      id: "55783c71-e104-46db-a2da-8b5fd7a23e1c",
      name: "colobus",
      type: "Dachshund",
      img: require("../../images/dachshund1.jpg")
    },
    {
      id: "297eb4a2-1d3e-4d52-a6af-2b926055aabf",
      name: "Woodchuck",
      type: "Dachshund",
      img: require("../../images/dachshund2.jpg")
    },
    {
      id: "d2ab2456-e035-4a18-b5e6-53d251deb260",
      name: "Silver",
      type: "Akita",
      img: require("../../images/akita1.jpg")
    },
    {
      id: "b955b561-75b1-4fa5-b580-d2d9a90ecb69",
      name: "Greylag",
      type: "Akita",
      img: require("../../images/akita2.jpg")
    },
    {
      id: "785cdc92-a8b6-41c2-85df-f5b1547c0c17",
      name: "sparrow",
      type: "Akita",
      img: require("../../images/akita3.jpg")
    },
    {
      id: "32b34031-6725-4751-baf2-15b4f6975110",
      name: "Glossy",
      type: "Akita",
      img: require("../../images/akita4.png")
    }
  ];

  function handleSubmitForm(dog){
    console.log(dog);
  }
  return (
    <div>
      <Container>
        <Typography variant="h1" component="h2" gutterBottom>
          Dog's Zoo
        </Typography>
        <Grid container spacing={3} style={{ margin: "auto" }}>
          <Grid item xs={12}>
            <AddDogForm handleSubmitForm={handleSubmitForm}/>
          </Grid>
          {Dogs.map(dog => {
            return (
              <Grid item md={3} key={dog.id}>
                <DogCard dog={dog} />
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </div>
  );
}
